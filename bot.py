# Soundboard bot by Jankyv

import config
import discord
from discord.ext import commands

# Also install ffmpeg https://blog.gregzaal.com/how-to-install-ffmpeg-on-windows/

# global variables
prefix = config.prefix
Client = discord.Client()
bot = commands.Bot(command_prefix=prefix)
all_sounds = {}

# Events
@bot.event
async def on_ready():
    await load_sounds()
    print("Ready when you are!")


@bot.event
async def on_message(message):
    message.content = message.content.lower().strip()
    # Check if the message isn't from the bot itself
    if message.author.bot:
        return

    # Ignore message if it doesn't start with the right prefix or message is empty
    if not message.content.startswith(prefix) or len(message.content) < 1:
        return

    if message.content.startswith(prefix + 'play'):
        context = await bot.get_context(message)
        await play(context)
        return

    elif message.content.startswith(prefix + 'leave'):
        context = await bot.get_context(message)
        await leave(context)
        return

    elif message.content.startswith(prefix + 'commands'):
        context = await bot.get_context(message)
        await show_commands(context)
        return

    elif message.content.startswith(prefix + 'sounds'):
        context = await bot.get_context(message)
        await list_sounds(context)
        return

    elif message.content.startswith(prefix + 'reload'):
        await load_sounds()
        return

    else:
        await message.channel.send("Deze command bestaat niet")


@bot.event
async def on_voice_state_update(member, before, after):
    has_members = False
    # voice_channel = before.channel
    # members = voice_channel.members

    # if before.channel != after.channel:
        # pass

    # print(member)
    # print(before)
    # print(after)


# Commands
@bot.command()
async def play(ctx):

    if await join(ctx):
        vc = ctx.voice_client
        arg = ctx.message.content[6:]
        try:
            arg = int(arg)
            if int(arg) in all_sounds:
                current_sound = all_sounds[arg]
                if vc.is_playing():
                    vc.stop()

                await ctx.channel.send("Speelt \'" + current_sound['name'] + "\'")
                vc.play(discord.FFmpegPCMAudio(current_sound['source']))
            else:
                await ctx.channel.send("Dit nummer bestaat niet.")
        except Exception as e:
            print(e)
            await ctx.channel.send("Er ging iets fout.")
    else:
        return


@bot.command()
async def leave(ctx):
    voice_client = ctx.voice_client

    if voice_client:
        if voice_client.is_connected():
            await voice_client.disconnect()
    else:
        return True


@bot.command()
async def show_commands(ctx):
    embed = discord.Embed(title="Soundboard", description="A very nice meme bot. List of commands are:", color=0xeee657)

    embed.add_field(name=prefix+"play X", value="Plays song with number X", inline=False)
    embed.add_field(name=prefix+"leave", value="Disconnect bot from voice channel", inline=False)
    embed.add_field(name=prefix+"reload", value="Reloads the sounds.txt. Use if songs are added", inline=False)
    embed.add_field(name=prefix+"songs", value="Show list of available songs with their indicator", inline=False)

    await ctx.channel.send(embed=embed)


@bot.command()
async def list_sounds(ctx):
    embed = discord.Embed(title="Soundboard", description="A very nice meme bot. List of sounds:", color=0xeee657)
    sounds_list = ""

    for key, value in all_sounds.items():
        sounds_list += str(key) + ". " + value['name'] + "\n"

    embed.add_field(name='Sounds', value=sounds_list, inline=True)

    await ctx.channel.send(embed=embed)


# Functions
async def join(ctx):
    channel = ctx.author.voice
    voice_client = ctx.voice_client

    if channel is not None:
        channel = channel.channel

        if voice_client:
            if voice_client.is_connected():
                return True
            else:
                await channel.connect()
                return True
        else:
            await channel.connect()
            return True
    else:
        await ctx.channel.send("Je moet eerst met een spraakkanaal verbinden")
        return False


async def load_sounds():
    with open("sounds.txt") as f:
        for line in f:
            key, source, name = line.split(' | ')
            all_sounds[int(key)] = {'source': source.strip(), 'name': name.strip()}


# run discord client with api key
bot.run(config.api_key)
