# Soundboard

Soundboard is a Discord bot written in Python with funny mememy sounds.

## Installation

Install the following packages or libraries on your server.

```bash
python3 -m pip install -U discord.py[voice]

Also install ffmpeg via https://blog.gregzaal.com/how-to-install-ffmpeg-on-windows/
```

## Usage

Use the discord link to add the bot to your discord server.

```python
https://discord.com/api/oauth2/authorize?client_id=715197084780855336&permissions=103885888&scope=bot
```

Contributing
Sound suggestions are welcome! If you have a mp3 link, please share!